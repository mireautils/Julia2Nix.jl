inputs@{
  stdenvNoCC,
  lib,
  symlinkJoin,
  callPackage,
  git,
  writers,
  ...
}: {julia2nix, ...}: let
  depots = lib.fold (a: b: a // b) {}
    (lib.mapAttrsToList 
      (method: lib.mapAttrs (_: inputs."${method}"))
      (lib.importTOML julia2nix).depot.${stdenvNoCC.system});

  srcs = lib.mapAttrs (n: v: let
    path = lib.replaceStrings ["-"] ["/"] v.name;
    patches = import ./patches.nix {inherit path;};
    name' = lib.last (lib.splitString "-" n);
    patch-context = lib.hasAttrByPath ["${name'}"] patches;
  in
    stdenvNoCC.mkDerivation {
      name = n;
      src = v;
      dontConfigure = true;
      dontBuild = true;
      dontFixup = true;
      sourceRoot = ".";
      unpackPhase = ''
        unpackDir="$TMPDIR/unpack"
        unpackFile "$src"
        cp -r "$src" "package"
      '';
      installPhase =
        ''
          runHook preInstall

          mkdir -p $out/${path}
          cp -rf --no-preserve=mode,ownership package/* $out/${path}
          runHook postInstall
        ''
        + lib.optionalString patch-context (let
          context = writers.writeBash "update-epgstation" patches."${name'}";
        in ''
          cp -rf --no-preserve=mode,ownership ${context} $out/${path}/patched.bash
        '');
    })
  depots;
in
  symlinkJoin {
    name = "julia-depot";
    paths = lib.attrValues srcs;
  }
